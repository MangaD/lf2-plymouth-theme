#!/bin/bash
 
## Preview Plymouth Splash
## by MangaD
## https://bitbucket.org/MangaD/
## License: MIT

# Run "sudo apt-get install plymouth-x11" first

if [ $(dpkg-query -W -f='${Status}' plymouth-x11 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Run 'sudo apt-get install plymouth-x11' first.";
  exit;
fi
 
chk_root () {
 
  if [ ! $( id -u ) -eq 0 ]; then
    echo Must be run as root
    exit
  fi
 
}
 
chk_root
 
DURATION=$1
 
if [ $# -ne 1 ]; then
	DURATION=5
fi
 
plymouthd; # Start Plymouth daemon
plymouth --show-splash; # Show the Plymouth window
plymouth pause-progress;
plymouth message --text="pausing boot - press 'c' or space bar to continue";
plymouth watch-keystroke --keys="cC " --command="tee /tmp/c_key_pressed";
plymouth message --text="resuming boot - wait $DURATION seconds.";
plymouth unpause-progress;
plymouth --update="Very looooooooooooooooooooooooooooooooooooooooooooooooooooooong line.";

for ((I=0; I<$DURATION; I++)); do
	plymouth --update=test$I;
	sleep 1;
done;

plymouth message --text="ask password - input password.";
plymouth --ask-for-password;

plymouth quit