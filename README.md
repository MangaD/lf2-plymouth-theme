LF2 - Plymouth Theme
=========

Very simple LF2 Plymouth Theme (may contain bugs).

![screenshot](https://bytebucket.org/MangaD/lf2-plymouth-theme/raw/519e09cfb23af552dca2247727d824f6064b4daf/preview.png)


### Installation

- **Move** 'lf2-davis' folder to `/usr/share/plymouth/themes`.

- Open a terminal and **paste**:
```
sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/lf2-davis/lf2-davis.plymouth 100
```

- Then **run**:
 
`sudo update-alternatives --config default.plymouth`

And **select lf2-davis**.

- Finally **paste**:

`sudo update-initramfs -u`


### License

[Little Fighter 2](http://lf2.net/) is property of Marti Wong and Starsky Wong.

Artwork belongs to [Henry The Archer](http://henry-the-archer.deviantart.com/).

This project is licensed with MIT. See LICENSE for more details.